Issue references:

* [Atlassian ServiceDesk](https://getsupport.atlassian.com/servicedesk/customer/portal/11/BBS-90164)
* [JIRA Team Connect](https://bringmeister.atlassian.net/browse/CONNECT-242)
* [GitHub Issue `testcontainers`](https://github.com/testcontainers/testcontainers-java/issues/892)
* [Atlassian Issue `testcontainer`](https://bitbucket.org/site/master/issues/17236/testcontainers-stoped-working)

Run the following steps to reproduce the issue:
```
docker run --privileged --name bitbucket-docker -d -p 127.0.0.1:2375:2375 --memory=1g --memory-swap=1g --memory-swappiness=0  atlassian/pipelines-docker-daemon:prod-stable
gradlew check -i
```

When run against `atlassian/pipelines-docker-daemon:prod-stable` the test should fail and log output similar to the following:
```
10:37:48.034 [Test worker] DEBUG org.testcontainers.utility.RegistryAuthLocator - Looking up auth config for image: quay.io/testcontainers/ryuk:0.2.2
10:37:48.037 [Test worker] DEBUG org.testcontainers.utility.RegistryAuthLocator - RegistryAuthLocator has configFile: /root/.docker/config.json (does not exist) and commandPathPrefix: 
10:37:48.038 [Test worker] WARN org.testcontainers.utility.RegistryAuthLocator - Failure when attempting to lookup auth config (dockerImageName: quay.io/testcontainers/ryuk:0.2.2, configFile: /root/.docker/config.json. Falling back to docker-java default behaviour. Exception message: /root/.docker/config.json (No such file or directory)
10:37:48.038 [Test worker] DEBUG org.testcontainers.dockerclient.auth.AuthDelegatingDockerClientConfig - Effective auth config [null]
10:37:48.069 [Test worker] DEBUG com.github.dockerjava.core.command.AbstrDockerCmd - Cmd: com.github.dockerjava.core.command.CreateContainerCmdImpl@26e501a4[name=testcontainers-ryuk-c93bcdef-e76e-4efb-a14c-f75b80126599,hostName=<null>,domainName=<null>,user=<null>,attachStdin=<null>,attachStdout=<null>,attachStderr=<null>,portSpecs=<null>,tty=<null>,stdinOpen=<null>,stdInOnce=<null>,env=<null>,cmd=<null>,entrypoint=<null>,image=quay.io/testcontainers/ryuk:0.2.2,volumes=com.github.dockerjava.api.model.Volumes@2adbe8f2,workingDir=<null>,macAddress=<null>,networkDisabled=<null>,exposedPorts=com.github.dockerjava.api.model.ExposedPorts@25d78fd1,stopSignal=<null>,hostConfig=com.github.dockerjava.api.model.HostConfig@394bd4fa[binds=com.github.dockerjava.api.model.Binds@7f465b10,blkioWeight=<null>,blkioWeightDevice=<null>,blkioDeviceReadBps=<null>,blkioDeviceReadIOps=<null>,blkioDeviceWriteBps=<null>,blkioDeviceWriteIOps=<null>,memorySwappiness=<null>,capAdd=<null>,capDrop=<null>,containerIDFile=<null>,cpuPeriod=<null>,cpuShares=<null>,cpuQuota=<null>,cpusetCpus=<null>,cpusetMems=<null>,devices=<null>,diskQuota=<null>,dns=<null>,dnsSearch=<null>,extraHosts=<null>,links=<null>,logConfig=<null>,lxcConf=<null>,memory=<null>,memorySwap=<null>,memoryReservation=<null>,kernelMemory=<null>,networkMode=<null>,oomKillDisable=<null>,autoRemove=true,oomScoreAdj=<null>,portBindings=<null>,privileged=<null>,publishAllPorts=true,readonlyRootfs=<null>,restartPolicy=<null>,ulimits=<null>,volumesFrom=<null>,pidMode=<null>,securityOpts=<null>,cgroupParent=<null>,volumeDriver=<null>,shmSize=<null>,pidsLimit=<null>,runtime=<null>,tmpFs=<null>],labels={org.testcontainers=true},networkingConfig=<null>,ipv4Address=<null>,ipv6Address=<null>,aliases=<null>,authConfig=<null>,execution=com.github.dockerjava.core.exec.CreateContainerCmdExec@2c21343a]
10:37:48.271 [Test worker] DEBUG com.github.dockerjava.core.command.AbstrDockerCmd - Cmd: d23d44d8234ae9c4bf8a0589b9c8a079a4d468b67aaad0f8ea5e3a1893fe8a99,com.github.dockerjava.core.exec.StartContainerCmdExec@65b87cab
10:37:48.664 [Test worker] DEBUG com.github.dockerjava.core.command.AbstrDockerCmd - Cmd: d23d44d8234ae9c4bf8a0589b9c8a079a4d468b67aaad0f8ea5e3a1893fe8a99,false,com.github.dockerjava.core.exec.InspectContainerCmdExec@4faf99d2
10:37:48.665 [Test worker] DEBUG com.github.dockerjava.core.exec.InspectContainerCmdExec - GET: OkHttpWebTarget(okHttpClient=org.testcontainers.shaded.okhttp3.OkHttpClient@2597e289, baseUrl=http://localhost:2375/, path=[/containers/d23d44d8234ae9c4bf8a0589b9c8a079a4d468b67aaad0f8ea5e3a1893fe8a99/json], queryParams={})
10:37:54.599 [testcontainers-ryuk] WARN org.testcontainers.utility.ResourceReaper - Can not connect to Ryuk at localhost:32768
java.net.ConnectException: Connection refused (Connection refused)
    at java.net.PlainSocketImpl.socketConnect(Native Method)
    at java.net.AbstractPlainSocketImpl.doConnect(AbstractPlainSocketImpl.java:350)
    at java.net.AbstractPlainSocketImpl.connectToAddress(AbstractPlainSocketImpl.java:206)
    at java.net.AbstractPlainSocketImpl.connect(AbstractPlainSocketImpl.java:188)
    at java.net.SocksSocketImpl.connect(SocksSocketImpl.java:392)
    at java.net.Socket.connect(Socket.java:589)
    at java.net.Socket.connect(Socket.java:538)
    at java.net.Socket.<init>(Socket.java:434)
    at java.net.Socket.<init>(Socket.java:211)
    at org.testcontainers.utility.ResourceReaper.lambda$start$1(ResourceReaper.java:112)
    at java.lang.Thread.run(Thread.java:748)
...

de.bringmeister.testcases.con242.TestContainersIntegrationTest > testTestContainerIntegrationWithBitbucketPipeline FAILED
    java.lang.IllegalStateException: Can not connect to Ryuk
        at org.testcontainers.utility.ResourceReaper.start(ResourceReaper.java:148)
        at org.testcontainers.utility.ResourceReaper.start(ResourceReaper.java:64)
        at org.testcontainers.DockerClientFactory.client(DockerClientFactory.java:124)
        at org.testcontainers.containers.GenericContainer.<init>(GenericContainer.java:162)
        at de.bringmeister.testcases.con242.TestContainersIntegrationTest.testTestContainerIntegrationWithBitbucketPipeline(TestContainersIntegrationTest.java:35)
```

When repeated and run against the _real_ Docker daemon the test is green.
