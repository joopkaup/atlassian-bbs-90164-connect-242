package de.bringmeister.testcases.con242;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.dockerclient.DockerClientProviderStrategy;
import org.testcontainers.dockerclient.NpipeSocketClientProviderStrategy;
import org.testcontainers.utility.TestcontainersConfiguration;

import java.util.List;
import java.util.ServiceLoader;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.assertj.core.api.Assertions.assertThat;

public class TestContainersIntegrationTest {

    private static final Logger LOG = LoggerFactory.getLogger(TestContainersIntegrationTest.class);

    @Test
    public void testTestContainerIntegrationWithBitbucketPipeline() {

        // Note for Windows:
        // On Windows we must ensure <WindowsClientProviderStrategy> is used
        // in order to connect to tcp://localhost:2375 without TLS.
        // Docker's configuration option "Expose daemon on tcp://localhost:2375 without TLS" must
        // *NOT* be enabled. Instead, we expose <atlassian/pipelines-docker-daemon> on localhost:2375.
        LOG.warn(
            "Connecting to [{}] using [{}]. Will this be Bitbucket's Runner?",
            System.getenv("DOCKER_HOST"),
            TestcontainersConfiguration.getInstance().getDockerClientStrategyClassName());

        try (GenericContainer container = new GenericContainer<>("redis:4.0.11-alpine")) {
            // We don't even reach here.
            // The previous call throws after being unable to connect to Ryuk.
            //
            // > 11:39:28.147 [testcontainers-ryuk] WARN org.testcontainers.utility.ResourceReaper - Can not connect to Ryuk at localhost:32768
            // > java.net.ConnectException: Connection refused: connect
            // > at java.net.DualStackPlainSocketImpl.connect0(Native Method)
            // > at java.net.DualStackPlainSocketImpl.socketConnect(DualStackPlainSocketImpl.java:79)
            // > at java.net.AbstractPlainSocketImpl.doConnect(AbstractPlainSocketImpl.java:350)
            // > at java.net.AbstractPlainSocketImpl.connectToAddress(AbstractPlainSocketImpl.java:206)
            // > at java.net.AbstractPlainSocketImpl.connect(AbstractPlainSocketImpl.java:188)
            // > at java.net.PlainSocketImpl.connect(PlainSocketImpl.java:172)
            // > at java.net.SocksSocketImpl.connect(SocksSocketImpl.java:392)
            // > at java.net.Socket.connect(Socket.java:589)
            // > at java.net.Socket.connect(Socket.java:538)
            // > at java.net.Socket.<init>(Socket.java:434)
            // > at java.net.Socket.<init>(Socket.java:211)
            // > at org.testcontainers.utility.ResourceReaper.lambda$start$1(ResourceReaper.java:112)
            // > at java.lang.Thread.run(Thread.java:748)

            container.start();

            assertThat(container.isRunning()).isTrue();
        }
    }

    @Test
    public void testIsNotUsingNamedPipeOnWindows() {
        // We ensure that testcontainers does not fallback to
        // <org.testcontainers.dockerclient.NpipeSocketClientProviderStrategy>
        // on Windows OS, which connects to the main Docker daemon instead of
        // Atlassian's.
        // Possible configuration locations:
        // * <C:\Users\<username>\.testcontainers.properties>
        // * Build in fallback if provided config is not available; also creates
        //   a file in user folder

        TestcontainersConfiguration config = TestcontainersConfiguration.getInstance();
        assertThat(config.getDockerClientStrategyClassName())
                .isNotEqualTo(NpipeSocketClientProviderStrategy.class.getCanonicalName());
    }
}
